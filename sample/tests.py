from django.test import TestCase
from sample.models import Animal
from django.contrib.gis.geos import Point


class AnimalTestCase(TestCase):
    def setUp(self):
        Animal.objects.create(name="lion", sound="roar", location=Point(1.521231, 47.123123))
        Animal.objects.create(name="cat", sound="meow", location=Point(47.123123, 1.521231))
        Animal.objects.create(name="dog", sound="ouaf", location=Point(47.123123, 1.521231))


    def test_animals_can_speak(self):
        """Animals that can speak are correctly identified"""
        lion = Animal.objects.get(name="lion")
        cat = Animal.objects.get(name="cat")
        self.assertEqual(lion.speak(), 'The lion says "roar"')
        self.assertEqual(cat.speak(), 'The cat says "meow"')
       
        
    def test_lion_speak(self):
        lion = Animal.objects.get(name="lion")
        self.assertEqual(lion.speak(), 'The lion says "roar"')


    def test_cat_speak(self):
        cat = Animal.objects.get(name="cat")
        self.assertEqual(cat.speak(), 'The cat says "meow"')
       
        
    def test_dog_speak(self):
        dog = Animal.objects.get(name="dog")
        self.assertEquals(dog.speak(), 'The dog says "ouaf"')
    
    
    def test_dog_location(self):
        dog = Animal.objects.get(name="dog")
        self.assertEquals(dog.locate(), "dog's location is SRID=4326;POINT (47.123123 1.521231)")
