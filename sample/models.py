# from django.db import models
from django.contrib.gis.db import models

# Create your models here.
class Animal(models.Model):
    name = models.CharField(max_length=30)
    sound = models.CharField(max_length=30)
    location = models.PointField(srid=4326)


    def speak(self):
        return 'The %s says "%s"' % (self.name, self.sound)

    
    def locate(self):
        return f"{self.name}'s location is {self.location}"