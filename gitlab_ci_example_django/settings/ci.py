# noinspection PyUnresolvedReferences
from .default import *

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'ci',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'postgis',
        'PORT': '5432',
    },
}

POSTGIS_TEMPLATE = 'postgis'